package schema

import (
	"encoding/json"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Log struct {
	Id          primitive.ObjectID `bson:"_id" json:"id"`
	JobUuid     string             `bson:"jobUuid,omitempty"`
	WorkerUuid  string             `bson:"workerUuid,omitempty"`
	Output      string             `bson:"output,omitempty"`
	IsPreScript bool               `bson:"isPreScript,omitempty"`
	Success     bool               `bson:"success,omitempty"`
	JobName     string             `bson:"jobName,omitempty"`
}

func (log Log) Database() string {
	return "log"
}

func (log Log) Collection() string {
	return "worker"
}

func (log Log) Key() string { // Key of redis queue
	return "logs"
}

func (log Log) MarshalBinary() ([]byte, error) {
	return json.Marshal(log)
}
