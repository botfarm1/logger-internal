packer {
  required_plugins {
    docker = {
      version = ">= 0.0.7"
      source  = "github.com/hashicorp/docker"
    }
  }
}

source "docker" "ubuntu" {
  image  = "golang:alpine"
  commit = true
  platform = "linux/amd64"
  changes = [
    "WORKDIR /go/app/cmd",
    "ENTRYPOINT /go/app/cmd/cmd"
  ]
}

build {
  name = "botfarm.azurecr.io/logger-internal"
  sources = [
    "source.docker.ubuntu"
  ]
  provisioner "file" {
    source = "."
    destination = "/go/app"
  }
  provisioner "shell" {
    inline = [
      "cd /go/app/cmd",
      "go get",
      "go build",
    ]
  }
  post-processors {
    post-processor "docker-tag" {
      repository = "botfarm.azurecr.io/logger-internal"
      tags       = ["latest"]
    }
  }
}
