package main

import (
	"gitlab.com/botfarm1/go/common/pkg/datastore"
	"gitlab.com/botfarm1/logger-internal/internal"
	"gitlab.com/botfarm1/logger-internal/pkg/schema"
	"log"
	"time"
)

const PollTime = time.Second * 4

func main() {
	logClient := datastore.RedisClient[schema.Log]{}
	logClient.Init()
	mongoClient := datastore.MongoClient[schema.Log]{}
	mongoClient.Init()

	// Listen for tasks in queue
	dispatcher := internal.Logger{
		PollTime:    PollTime,
		LogClient:   logClient,
		MongoClient: mongoClient,
	}

	err := dispatcher.Start()
	if err != nil {
		log.Fatalf(err.Error())
	}
}
