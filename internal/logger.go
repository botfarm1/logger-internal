package internal

import (
	"fmt"
	"gitlab.com/botfarm1/go/common/pkg/datastore"
	"gitlab.com/botfarm1/logger-internal/pkg/schema"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Logger struct {
	PollTime    time.Duration
	LogClient   datastore.RedisClient[schema.Log]
	MongoClient datastore.MongoClient[schema.Log]
}

func (logger Logger) Start() error {
	for {
		log, err := logger.LogClient.Dequeue()
		fmt.Println("Storing log: ", log)

		if err == nil {
			go logger.log(log) // Create thread to store log
		}
		time.Sleep(logger.PollTime)
	}
}

func (logger Logger) log(log schema.Log) {
	log.Id = primitive.NewObjectID()
	err := logger.MongoClient.Create(log)
	if err != nil {
		fmt.Println("Error occurred storing log: ", log.JobUuid, ". Pushing back to queue.", err)
		_ = logger.LogClient.Enqueue(log)
	}
}
